package com.example.monavissurtout.test;

import com.example.monavissurtout.model.review.Review;
import com.example.monavissurtout.model.review.ReviewList;
import com.example.monavissurtout.model.reviewfilter.FilterProximity;

import android.location.Location;
import android.test.AndroidTestCase;

public class Reviewfiltertest extends AndroidTestCase {

	protected ReviewList liste;
	
	
	public void setUp ( ) throws Exception {
		this.liste = new ReviewList();
		/* Avis 1 */
		Review r1 = new Review("r1");
		r1.setTitle("Avis1");
		r1.setContent("Le premier avis");
		r1.setAuthor("JB");
		r1.setRating(1);
		/* Gardanne (maison)*/
		r1.setLatitude(43.4554112);
		r1.setLongitude(5.4663849);
		r1.setAddress("Une adresse");
		/* Avis 2 */
		Review r2 = new Review("r2");
		r2.setTitle("Avis2");
		r2.setContent("Le deuxième avis");
		r2.setAuthor("JB");
		r2.setRating(2);
		/* CNAM */
		r2.setLatitude(43.5305369);
		r2.setLongitude(5.4555642);
		r2.setAddress("Une adresse");
		/* Avis 3 */
		Review r3 = new Review("r3");
		r3.setTitle("Avis3");
		r3.setContent("Le troisième avis");
		r3.setAuthor("JB");
		r3.setRating(3);
		/* Musée Granet */
		r3.setLatitude(43.5253834);
		r3.setLongitude(5.4528015);
		r3.setAddress("Une adresse");
		/* Avis 4 */
		Review r4 = new Review("r4");
		r4.setTitle("Avis4");
		r4.setContent("Le quatrième avis");
		r4.setAuthor("JB");
		r4.setRating(4);
		/* Bd Capitaine Geze Marseille */
		r4.setLatitude(43.3316155);
		r4.setLongitude(5.3706267);
		r4.setAddress("Une adresse");
		/* Avis 5 */
		Review r5 = new Review("r5");
		r5.setTitle("Avis5");
		r5.setContent("Le cinquième avis");
		r5.setAuthor("JB");
		r5.setRating(5);
		/* Plage de la couronne */
		r5.setLatitude(43.3325949);
		r5.setLongitude(5.0501617);
		r5.setAddress("Une adresse");
		
		liste.add(r1);
		liste.add(r2);
		liste.add(r3);
		liste.add(r4);
		liste.add(r5);
	}

	public void testNbReview1(){
		FilterProximity filter = new FilterProximity(2);
		Location location = new Location("");
		location.setLatitude(43.5262152);
		location.setLongitude(5.4454590);
		assertEquals(filter.filter(liste, location).size(), 2);
	}
	
	public void testNbReview2(){
		FilterProximity filter = new FilterProximity(6);
		Location location = new Location("");
		location.setLatitude(43.5262152);
		location.setLongitude(5.4454590);
		assertEquals(filter.filter(liste, location).size(), 5);
	}
	
	public void testPlusProche(){
		FilterProximity filter = new FilterProximity(2);
		Location location = new Location("");
		location.setLatitude(43.5262152);
		location.setLongitude(5.4454590);
		ReviewList listeFilter = filter.filter(liste, location);
		assertEquals(listeFilter.get(0).getTitle(), "Avis3");
		assertEquals(listeFilter.get(1).getTitle(), "Avis2");
	}
}
